package jp.alhinc.yano_marina.calculate_salse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;



public class CalculateSales {

	public static void main(String[] args) {
		//支店一覧を呼び出す
		//System.out.println("支店一覧をひらく" + args[0]);

		//マップを作成
		HashMap<String, Long> salesmap = new HashMap<String, Long>();
		HashMap<String, String> branchmap = new HashMap<String, String>();
		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			//支店定義ファイルがフォルダに存在するか確認
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			br = new BufferedReader(new FileReader(file));

			String line;
			while((line = br.readLine()) != null) {
				//lineをコンマで分割
				String[] branchLine = line.split(",");
				if(branchLine.length != 2 || !branchLine[0].matches("[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				//[0]は支店コード、[1]は支店名
				branchmap.put(branchLine[0], branchLine[1]);
				//マップ2に支店コードと値0を入れておく
				//0はロング型にしておく
				salesmap.put(branchLine[0], 0L);
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		//売上ファイルを読み込む
		//売上ファイルは拡張子がrcdで数字が８桁のもの、かつ、ファイル
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String str) {
				File file = new File(dir, str);
				if(str.matches("\\d{8}.rcd") && file.isFile()) {
					return true;
				}else {
					return false;
				}
			}
		};
		//フィルタを作成して取り出す
		File[] files = new File(args[0]).listFiles(filter);

        //該当するファイルをひとつ取り出す
		//ファイルの読み込みを繰り返す
		for(int i = 0; i < files.length; i++) {
			//files[i]のファイル名を取得
			String nowFileName = files[i].getName();
			//files[i]のファイル名と拡張子を分ける
			String[] nowFileNumber = nowFileName.split("\\.");
			//ファイル名をint型に変更する
			int nowNumber = Integer.parseInt(nowFileNumber[0]);
			if(i != 0) {
				//files[i-1]のファイル名を出す
				String preFileName = files[i-1].getName();
				String[] preFileNumber = preFileName.split("\\.");
				int preNumber = Integer.parseInt(preFileNumber[0]);

				if(nowNumber - preNumber != 1){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			//支店コードと売上額を抽出
			try {
				FileReader fr = new FileReader(files[i]);
				br = new BufferedReader(fr);

				//ファイルの中身を配列に組み込む
				ArrayList<String> rcdFileNumbers = new ArrayList<String>();
				String line;
				while((line = br.readLine()) != null) {
					rcdFileNumbers.add(line);
				}
				//売上ファイルの行数を配列数で確認
				if(rcdFileNumbers.size() != 2) {
					System.out.println(nowFileName+"のフォーマットが不正です");
					return;
				}
				//支店コードに該当がない場合
				if(!branchmap.containsKey(rcdFileNumbers.get(0))) {
					System.out.println(nowFileName+"の支店コードが不正です");
					return;
				}

				//売上金額をロング型に変換
				long sales = Long.parseLong(rcdFileNumbers.get(1));

				//同じキーの値を足し合わせる
				Long current = salesmap.get(rcdFileNumbers.get(0));
				long sumSales = sales + current;
				salesmap.put(rcdFileNumbers.get(0), sumSales);
				//合計金額が10桁を超えたら処理を終了する
				long sumLength = String.valueOf(sumSales).length();
				if(sumLength > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//支店コード、支店名、合計金額をファイルに出力する
		BufferedWriter bw = null;
		try {
			File file = new File(args[0], "branch.out");
			bw = new BufferedWriter(new FileWriter(file));
			for(String keyList : branchmap.keySet()) {
				String sameSales = String.valueOf(salesmap.get(keyList));
				bw.write(keyList + "," + branchmap.get(keyList) + "," + sameSales);
				bw.newLine();
			}
			bw.close();
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}


	}
}






